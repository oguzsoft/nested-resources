## Nested Resources App 

* user has many posts and post belongs to user.

```bash
rails new nested-resources
```

```
cd nested-resources
```

* Open Gemfile and add those gems

```ruby
gem 'devise'
gem 'hirb
```

```bash
bundle install
```

#### Adding Devise

```bash
rails g devise:install
```

#### Configure Devise
  * Open up ```config/environments/development.rb``` and add this line ```config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }```
   before the ```end``` keyword.

* Open up ```app/views/layouts/application.html.erb```

```ruby
<% if notice %>
  <p class="alert alert-success"><%= notice %></p>
<% end %>
<% if alert %>
  <p class="alert alert-danger"><%= alert %></p>
<% end %>
 <p class="navbar-text pull-right">
    <% if user_signed_in? %>
      Logged in as <strong><%= current_user.email %></strong>.
      <%= link_to 'Edit profile', edit_user_registration_path, :class => 'navbar-link' %> |
      <%= link_to "Logout", destroy_user_session_path, method: :delete, :class => 'navbar-link'  %>
    <% else %>
      <%= link_to "Sign up", new_user_registration_path, :class => 'navbar-link'  %> |
      <%= link_to "Login", new_user_session_path, :class => 'navbar-link'  %>
    <% end %>
    </p>

```

#### Setup the devise model

```bash
rails g devise user
```

#### Migrating database

```bash
rails db:migrate
```

#### Scaffolding Post

```bash
rails g scaffold Post content
```

* Migrating database

```bash
rails db:migrate
```

#### Add Migrate

```bash
rails g migration add_user_id_to_post user_id:integer:index
```

```bash
rails db:migrate
```

* Add user_id param in posts.controller.rb

```ruby
def post_params
  params.require(:post).permit(:content)
end
```

* In ```models/user.rb```

```ruby
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :posts
end
```

* In ```models/post.rb```

```ruby
class Post < ApplicationRecord
	belongs_to :user
end
```

### Configure nested resources in routes.rb look like this

```ruby
Rails.application.routes.draw do
  
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :users do 
  	resources :posts
  end

  root 'posts#index'
end
```

### Finaly

* Modified some file
	- ```_form.html.erb```
	- ```index.html.erb```
	- ```posts.controller.rb```
	- ```edit.html.erb```
	- ```new.html.erb```
















